<<<<<<< HEAD
#include "WiFi.h" // Enables the ESP32 to connect to the local network (via WiFi)
#include <PubSubClient.h> // Allows us to connect to, and publish to the MQTT broker
#include <Adafruit_NeoPixel.h> // Controlls the LED strips on the Grid

=======
#include "WiFi.h"
#include <PubSubClient.h>
#include <NeoPixelBus.h>
>>>>>>> arduinocode

int sensorPin = A6;
int sensorPin2 = A7;
int sensorPin3 = A4;
int sensorPin4 = A5;

bool sensorChange = true; //true if theres a person under the sensor

<<<<<<< HEAD
int pin = 14;
int numpixels = 17*17;
=======
const uint16_t PixelCount = 17*17;
const uint16_t PixelPin = 22;

const int payloadsize = PixelCount*6;
String grid_array[payloadsize/6]; //array of the hex values in the grid
>>>>>>> arduinocode

bool debug = true;
int prev = 0;
int prev2 = 0;
int prev3 = 0;
int prev4 = 0;

Adafruit_NeoPixel pixels(numpixels, pin, NEO_GRB + NEO_KHZ800);

// WiFi
const char* ssid = "Tesla IoT";
const char* wifi_password = "fsL6HgjN";

// MQTT
const char* mqtt_server = "192.168.43.246";
const char* mqtt_topic = "test";
const char* mqtt_topic2 = "test2";
const char* mqtt_username = "user";
const char* mqtt_password = "komkommer";
const char* clientID = "ESP32_1";

// Initialise the WiFi and MQTT Client objects
WiFiClient wifiClient;
PubSubClient client(mqtt_server, 1883, wifiClient); // 1883 is the listener port for the Broker

void setup() {
  if(debug){
    Serial.begin(115200);
    Serial.print("Connecting to ");
    Serial.println(ssid);
  }
  // connect to the WiFi
  WiFi.begin(ssid, wifi_password);

  // Wait until the connection has been confirmed before continuing
  while (WiFi.status() != WL_CONNECTED) {
    if(debug) {
      delay(500);
      Serial.print(".");
    }
  }

  // Debugging - Output the IP Address of the ESP8266
  if(debug) {
      Serial.println("WiFi connected");
      Serial.print("IP address: ");
      Serial.println(WiFi.localIP());
  }

  // Connect to MQTT Broker
  client.setCallback(ReceivedMessage); // setCallback sets the function to be called when a message is received.
  
  // connect() returns a boolean value to let us know if the connection was successful.
  if (connect()) {
    if(debug){
      Serial.println("Connected Successfully to MQTT Broker!"); 
    }        
  } else {
    if(debug){
      Serial.println("Connection Failed!");
    }
  }
<<<<<<< HEAD
  pixels.begin();
=======
  pixels.Begin(); //initialise grid
>>>>>>> arduinocode
}

void loop() {
  if (!client.connected()) { //checks connection to broker
    connect(); //connects if not connected
  }
  checkMovement(sensorPin);
  client.loop();
}

void ReceivedMessage(char* topic, byte* payload, unsigned int length) {
<<<<<<< HEAD
  pixels.clear();
  // Output the first character of the message to serial (debug)
  const int payloadsize = numpixels*6;
  String grid_array[payloadsize/6];
  for (int i = 0; i < payloadsize; i++){
    int grid_index = i/6;
    grid_array[grid_index] += (char)payload[i];
    //Serial.print((char)payload[i]);
=======
  String hex;
  for (int i = 0; i < payloadsize; i++){ //loops through the received message
    int grid_index = i/6; //payload contains hex values, 6 characters
    hex += (char)payload[i];
    if(hex.length() == 6){
      grid_array[grid_index] = hex; //combines the characters to create the hex value
      hex = "";
    }
>>>>>>> arduinocode
  }
  writeGrid(grid_array);
}

bool connect() {
  // connect to MQTT Server and subscribe to the topic
  if (client.connect(clientID, mqtt_username, mqtt_password)) {
      client.subscribe(mqtt_topic2);
      return true;
    }
    else {
      return false;
  }
}

void writeGrid(String hex[]){
<<<<<<< HEAD
  for (int i = 0; i < 17; i++){
=======
  for (int i = 0; i < 17; i++){ //loops through grid
>>>>>>> arduinocode
    for (int j = 0; j < 17; j++){
      long number = (long)strtol(hex[j+i*17].c_str(), NULL, 16); //converts the hex string to long
      //hex conversion to rgb
      int r = number >> 16;
      int g = number >> 8 & 0xFF;
      int b = number & 0xFF;
<<<<<<< HEAD
      Serial.print(r);
      Serial.print(g);
      Serial.print(b);
      Serial.print(" ");
=======
      if(debug){
      //  Serial.print(r);
      //  Serial.print(g);
      //  Serial.print(b);
      //  Serial.print(" ");
      }
      //reverse each uneven row of the grid because its soldered that way
>>>>>>> arduinocode
      if (i%2){
        pixels.setPixelColor(16-j+i*17, pixels.Color(r, g, b));
      }
      else {
        pixels.setPixelColor(j+i*17, pixels.Color(r, g, b));
      }
    }
    if(debug){
     // Serial.println("");
    }
  }
<<<<<<< HEAD
  Serial.println("message received");
  pixels.show();
=======
  pixels.Show();
>>>>>>> arduinocode
}

void checkMovement(int sensor){
  int sensorValue = 0;
  int sensorValue2 = 0;
  int sensorValue3 = 0;
  int sensorValue4 = 0;
  int average = 3000;
  int temp;
  
  for (int i = 0; i < average; i++) {
    sensorValue += analogRead(sensorPin);
    sensorValue2 += analogRead(sensorPin2);
    sensorValue3 += analogRead(sensorPin3);
    sensorValue4 += analogRead(sensorPin4);
  }

  sensorValue = sensorValue / average;
  sensorValue2 = sensorValue2 / average;
  sensorValue3 = sensorValue3 / average;
  sensorValue4 = sensorValue4 / average;

  Serial.print("1 - ");
  checkValue(sensorValue, 8, 8);
  Serial.print("2 - ");
  checkValue(sensorValue2, 17, 8);
  Serial.print("3 - ");
  checkValue(sensorValue3, 8, 17);
  Serial.print("4 - ");
  checkValue(sensorValue4, 17, 17);
}

void checkValue(int sensorValue, int x, int y){
  if(sensorValue > 350) {// &&  sensorChange){ //person detected, need sensorchange variable for each sensor
    client.publish(mqtt_topic, "1");
    for(int i = 0; i < 17; i++){ //change grid array to follow the person
      for(int j = 0; j < 17; j++){
        if(i>=x-8 && i<x && j>=y-8 && j<y){
          grid_array[i + 17*j] = "001100";
        }
      }
    }
    writeGrid(grid_array);
    if(debug){
      Serial.println("message sent!" + String(sensorValue));
    }
    sensorChange = false;
  }
  else if(sensorValue < 400) {// && !sensorChange){ //the person left
    client.publish(mqtt_topic, "0");
    for(int i = 0; i < 17; i++){
      for(int j = 0; j < 17; j++){
        if(i>=x-8 && i<x && j>=y-8 && j<y){
          grid_array[i + 17*j] = "000000";
        }
      }
    }
    writeGrid(grid_array);
    if(debug){
      Serial.println("message sent! Value: " + String(sensorValue));
    }
    sensorChange = true;
  }
}