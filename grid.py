#!/usr/bin/env python3


#-----------------------------------------------------------------------------
# libraries
#-----------------------------------------------------------------------------

import time
import subprocess
import pyowm    # weather api
import random
import paho.mqtt.client as mqtt #import the client1

#-----------------------------------------------------------------------------
# Global variables
#-----------------------------------------------------------------------------

username = "user"
password = "komkommer"
topic = "test"
topic2 = "test2"
broker_address = "145.24.238.42"
#broker_address = "test.mosquitto.org"

frame = 1

#-----------------------------------------------------------------------------
# functions to initiate pannel grid
#-----------------------------------------------------------------------------

def make_grid(grid_colomns,grid_rows,module_colomns,module_rows,default_color):
    return [module_rows*grid_rows*[default_color] for i in range(grid_colomns*module_colomns)]

#-----------------------------------------------------------------------------
# functions to change view of pannel grid
#-----------------------------------------------------------------------------

def make_grid_weather(grid):
    global frame
#    weather = get_weather()
    weather="Rain"

    if weather == 'Gewad':
        if frame == 1:
            grid = make_grid_picture(grid, "art/fack_gerard1.jpg")
            frame = 0
        else:
            grid = make_grid_picture(grid, "art/fack_gerard2.jpg")
            frame = 1

    if weather == 'Clear':
        grid = make_grid_picture(grid, "art/sunny.jpg")
    if weather == 'Thunderstorm':
        if frame == 1:
            grid = make_grid_picture(grid, "art/thunder1.jpg")
            frame = 0
        else:
            grid = make_grid_picture(grid, "art/thunder2.jpg")
            frame = 1
    if weather == 'Drizzle':
        grid = make_grid_rain(grid)
    if weather == 'Rain':
        grid = make_grid_rain(grid)
    if weather == 'Snow':
        grid = make_grid_snow(grid)
    if weather == 'Mist':
        if frame == 0:
            grid = make_grid_picture(grid, "art/mist1.jpg")
            frame = 1
        elif frame == 1:
            grid = make_grid_picture(grid, "art/mist2.jpg")
            frame = 2
        else:
            grid = make_grid_picture(grid, "art/mist3.jpg")
            frame = 0
    if weather == 'Smoke':
        if frame == 0:
            grid = make_grid_picture(grid, "art/mist1.jpg")
            frame = 1
        elif frame == 1:
            grid = make_grid_picture(grid, "art/mist2.jpg")
            frame = 2
        else:
            grid = make_grid_picture(grid, "art/mist3.jpg")
            frame = 0
    if weather == 'Haze':
        if frame == 0:
            grid = make_grid_picture(grid, "art/mist1.jpg")
            frame = 1
        elif frame == 1:
            grid = make_grid_picture(grid, "art/mist2.jpg")
            frame = 2
        else:
            grid = make_grid_picture(grid, "art/mist3.jpg")
            frame = 0
    if weather == 'Dust':
        if frame == 0:
            grid = make_grid_picture(grid, "art/mist1.jpg")
            frame = 1
        elif frame == 1:
            grid = make_grid_picture(grid, "art/mist2.jpg")
            frame = 2
        else:
            grid = make_grid_picture(grid, "art/mist3.jpg")
            frame = 0
    if weather == 'Fog':
        if frame == 0:
            grid = make_grid_picture(grid, "art/mist1.jpg")
            frame = 1
        elif frame == 1:
            grid = make_grid_picture(grid, "art/mist2.jpg")
            frame = 2
        else:
            grid = make_grid_picture(grid, "art/mist3.jpg")
            frame = 0
    if weather == 'Sand':
        if frame == 0:
            grid = make_grid_picture(grid, "art/sand1.jpg")
            frame = 1
        elif frame == 1:
            grid = make_grid_picture(grid, "art/sand2.jpg")
            frame = 2
        else:
            grid = make_grid_picture(grid, "art/sand3.jpg")
            frame = 0
    if weather == 'Dust':
        if frame == 0:
            grid = make_grid_picture(grid, "art/sand1.jpg")
            frame = 1
        elif frame == 1:
            grid = make_grid_picture(grid, "art/sand2.jpg")
            frame = 2
        else:
            grid = make_grid_picture(grid, "art/sand3.jpg")
            frame = 0
    if weather == 'Ash':
        if frame == 0:
            grid = make_grid_picture(grid, "art/sand1.jpg")
            frame = 1
        elif frame == 1:
            grid = make_grid_picture(grid, "art/sand2.jpg")
            frame = 2
        else:
            grid = make_grid_picture(grid, "art/sand3.jpg")
            frame = 0
    if weather == 'Squall':
        if frame == 0:
            grid = make_grid_picture(grid, "art/mist1.jpg")
            frame = 1
        elif frame == 1:
            grid = make_grid_picture(grid, "art/mist2.jpg")
            frame = 2
        else:
            grid = make_grid_picture(grid, "art/mist3.jpg")
            frame = 0
    if weather == 'Tornado':
        if frame == 0:
            grid = make_grid_picture(grid, "art/mist1.jpg")
            frame = 1
        elif frame == 1:
            grid = make_grid_picture(grid, "art/mist2.jpg")
            frame = 2
        else:
            grid = make_grid_picture(grid, "art/mist3.jpg")
            frame = 0
    if weather == 'Clouds':
        if frame == 1:
            grid = make_grid_picture(grid, "art/cloud1.jpg")
            frame = 0
        else:
            grid = make_grid_picture(grid, "art/cloud2.jpg")
            frame = 1

    print(frame)
    return grid

def make_grid_lolcat(grid):
    for index_grid_colomn in range(len(grid[1])):
        for index_grid_row in range(len(grid)):
            old_color = grid[index_grid_row-1][index_grid_colomn-1]

            if int(old_color[0:2],16) > 16 or int(old_color[2:4],16) > 16 or int(old_color[4:6],16) > 16:
                color = "%06x" % random.randint(0, 0xFFFFFF)
                grid[index_grid_row-1][index_grid_colomn-1] = color
    return grid

def make_grid_rain(grid):
    for index_grid_colomn in range(len(grid[1])):
        for index_grid_row in range(len(grid)):
            if random.randint(0, 19) < 3:
                color = "%06x" % random.randint(0, 0xFF)
                grid[index_grid_row-1][index_grid_colomn-1] = color
    return grid

def make_grid_snow(grid):
    for index_grid_colomn in range(len(grid[1])):
        for index_grid_row in range(len(grid)):
            if random.randint(0, 19) < 3:
                color = "FFFFFF"
                grid[index_grid_row-1][index_grid_colomn-1] = color
    return grid

def get_weather():
    owm = pyowm.OWM('e5211a03e8bc4aabb6d8a6ffa15b317d')
    observation = owm.weather_at_place('Rotterdam,NL')
    w = observation.get_weather()
    return w.get_status()

def make_grid_picture(grid, picture):
    img = subprocess.check_output("./image_to_hex.sh " + picture, shell = True, universal_newlines=True)
    for i in range(len(grid[0])):
        for j in range(len(grid[1])):
            color = img[j*6 + 17*6*i :(j+1)*6 + 17*6 * i]

            if int(color[0:2], 16) > 16 or int(color[2:4], 16) > 16 or int(color[4:6], 16) > 16:
                grid[i][j] = color
    return grid

#-----------------------------------------------------------------------------
# mqtt
#-----------------------------------------------------------------------------
def on_message(client, userdata, message):
    print("message received", str(message.payload))
    print("Publishing message to topic", topic2)
    print(gridprint)
    client.publish(topic2, gridprint)

def on_connect(client, userdata, flags, rc):
    # rc is the error code returned when connecting to the broker
    print("Subscribing to topic", topic)
    client.subscribe(topic)

def on_publish(client,userdata,result):             #create function for callback
    print("data published \n")

#-----------------------------------------------------------------------------
# main
#-----------------------------------------------------------------------------

def main():
   # print(*grid, sep='\n')

    client = mqtt.Client() #create new instance
    client.username_pw_set(username, password)

    client.on_connect = on_connect
    client.on_message = on_message #attach function to callback
    client.on_publish = on_publish

    print("connecting to broker")
    client.connect(broker_address) #connect to broker

    while True:
        grid = make_grid(1,1,17,17,'000000')
#        grid = make_grid_weather(grid)
   #     grid = make_grid_picture(grid, "art/4chan.jpg")
        grid = make_grid_rain(grid)
    
        message = ''
        for index_grid_column in range(len(grid[0])):
            for index_grid_row in range(len(grid[1])):
                message += grid[index_grid_column][index_grid_row]
                #client.publish(topic2, grid[index_grid_column][index_grid_row])
#        print(message)
        client.publish(topic2, message)
        #time.sleep(0.05)    
#        time.sleep(1)    
    #    client.loop_forever()
    client.disconnect()

main()
