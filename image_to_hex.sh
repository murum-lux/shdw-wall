#!/bin/bash
# script to convert image to hex string

[ -z "$1" ] && echo "no parameter, exiting..." && exit

#filetype=$(file -b $1 | awk -F" " '{print $1}')
#[ "$filetype" != "JPEG" ] && \
#		 echo "Not a propper jpg file, exiting..." && exit

resolution=$(identify -format "%wx%h" $1)
[ "$resolution" != "17x17" ] && \
		echo -e -n "$resolution, Not a propper resolution, " && \
			echo "should be 17x17, Exiting..." && exit

convert $1 tmp.txt
cat tmp.txt | awk -F" |#" '{print $5}' | tr -d '\n' |\
		awk -F":" '{print $2}' | tr '[:upper:]' '[:lower:]'
